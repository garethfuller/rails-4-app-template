window.Views ||= {}
class Views.ApplicationView
  render: ->
    Widgets.Example.enable()
  cleanup: ->
    Widgets.Example.cleanup()
