pageLoad = ->
  className = $('body').attr('data-class-name')
  window.applicationView = try
    eval("new #{className}()")
  catch error
    new Views.ApplicationView()
  window.applicationView.render()

$ ->
  pageLoad()
  $(document).on 'page:before-change', ->
    window.applicationView.cleanup()
    true
  $(document).on 'page:restore', ->
    window.applicationView.cleanup()
    pageLoad()
    true
